@extends('layouts.app')

@section('content')
<section class="container">
<div class="mt-2">
    <div class="form-row justify-content-center">
    <div class="form-group col-md-6">
      
<h5 class="alert alert-info text-center">Ficha de afiliación</h5>
    </div>
    </div >

        <div class="form-row">
            <div class="form-group col-md-6">
             <div class="input-group mb-3 ">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Socio N°:</span>
              </div>
              <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            </div>
            <div class="form-group col-md-6">
             <div class="input-group mb-3 ">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Fecha:</span>
              </div>
              <input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            </div>

    </div>

</div>


<div><h5 class="text-danger text-center">Plan de Emergencias</h5></div>
<div class="card bg-light mb-3" >
  <div class="card-header text-info text-center"><b>Titular</b></div>
  <div class="card-body">
<form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="">Nombre:</label>
      <input type="text" class="form-control"  name="first_name" >
    </div>
    <div class="form-group col-md-6">
      <label for="">Apellido:</label>
      <input type="text" class="form-control" name="last_name">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-4">
    <label for="">Fecha de Nacimiento:</label>
    <input type="date" class="form-control" name="birth_date" >
  </div>
    <div class="form-group col-md-4">
    <label for="">D.N.I/L.C./L.E.:</label>
    <input type="text" class="form-control" name="dni">
  </div>
    <div class="form-group col-md-4">
    <label for="">Teléfono:</label>
    <input type="text" class="form-control" name="telephone" >
  </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-6">
    <label for="">Domicilio de Vivienda:</label>
    <input type="text" class="form-control" name="residence_address" >
  </div>
    <div class="form-group col-md-6">
    <label for="">Barrio:</label>
    <input type="text" class="form-control" name="neighborhood_residence" >
  </div>
  </div>
   <div class="form-row">
  <div class="form-group col-md-6">
    <label for="">Domicilio de Cobranza:</label>
    <input type="text" class="form-control" name="collection_address">
  </div>
    <div class="form-group col-md-6">
    <label for="">Barrio:</label>
    <input type="text" class="form-control" name="neighborhood_collection">
  </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="">Lugar de Trabajo</label>
      <input type="text" class="form-control" name="side_work" >
    </div>
    <div class="form-group col-md-3">
      <label for="">Obra Social</label>
      <input type="text" class="form-control" name="social_work" >
    </div>
    <div class="form-group col-md-3">
      <label for="">Email:</label>
      <input type="text" class="form-control" name="email" >
    </div>
  </div>
</form>
  </div>
</div>



<div class="card bg-light mb-3" >
  <div class="card-header text-info text-center"><b>Grupo Familiar</b></div>
  <div class="card-body">
<div class="table-responsive-sm">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col"><button type="button" class="btn btn-success btn-sm" id="newGroupFamily">+</button></th>
      <th scope="col">Apellido y Nombre</th>
      <th scope="col">Obra Social</th>
      <th scope="col">Parentesco</th>
      <th scope="col">DNI</th>
      <th scope="col">Fecha de Nac.</th>
      <th scope="col">-</th>
    </tr>
  </thead>
  <tbody id="bodyTable">
    <tr id="row-1">
      <th scope="row">1</th>
      <td>
        <div class="input-group">
          <input type="text" placeholder="Apellido" class="form-control" name="group_last_name[]">
          <input type="text" placeholder="Nombre" class="form-control" name="group_first_name[]">
        </div>
     </td>
      <td>
       <div class="form-group">
        <input type="text" class="form-control" placeholder="Obra social" name="group_social_work[]">
        </div>
     </td>
      <td>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Parentesco" name="group_relationship[]">
      </div>
    </td>
     <td>
     <div class="form-group">
        <input type="text" class="form-control" placeholder="DNI" name="group_dni[]">
      </div>
    </td>
     <td>
     <div class="form-group">
        <input type="date" class="form-control" placeholder="Fecha de Nacimiento" name="group_birth_date[]">
      </div>
    </td>
      <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('1');"><i class="fa fa-trash"></i></button></td>
    </tr>

  </tbody>
</table>
</div>
  </div>
</div>


</section>  


<script type="text/javascript">
	
var count=1;	
  $("#newGroupFamily").click(function (e) {

count++;

rowItem=`</tr>
  </thead>
  <tbody>
    <tr id="row-${count}">
      <th scope="row">${count}</th>
      <td>
      	<div class="input-group">
		  <input type="text" placeholder="Apellido" class="form-control" name="group_last_name[]" >
		  <input type="text" placeholder="Nombre" class="form-control"  name="group_first_name[]">
		</div>
	 </td>
      <td>
       <div class="form-group">
    	<input type="text" class="form-control" placeholder="Obra social"  name="group_social_work[]">
  		</div>
  	 </td>
      <td>
      <div class="form-group">
	    <input type="text" class="form-control" placeholder="Parentesco" name="group_relationship[]">
	  </div>
	</td>
	 <td>
	 <div class="form-group">
	    <input type="text" class="form-control" placeholder="DNI" name="group_dni[]">
	  </div>
	</td>
     <td>
     <div class="form-group">
	    <input type="date" class="form-control" placeholder="Fecha de Nacimiento" name="group_birth_date[]">
	  </div>
	</td>
      <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('${count}')"><i class="fa fa-trash"></i></button></td>
    </tr>`;

var body=document.getElementById('bodyTable');
body.innerHTML+=rowItem;
});

function deleteRow(id) {
	$("#row-"+id).remove();
}

</script>
@endsection
